# tim4nac.us
This is my campaign website for Code for America's National Advisory Council (first created for 2020 election).

# Contributing
Feel free to fill out an [Issue](https://gitlab.com/combinatorist/tim4nac.us/issues) to reach out to me about my website, campaign, or "policies".

Or, if you are comfortable with gitlab, you can of course make a pull request.
All my website content can be found under the `/content` directory in [Markdown](https://en.wikipedia.org/wiki/Markdown) files.
Within `/content`, my NAC application questions are under `/post` and if elected, I plan to make another section for `/policies`.

And, if you really want to get into it, you can learn about [Hugo](gohugo.io).
To develop the site locally, try (after installing):
```
	hugo server
```
