---
title: "About"
type: page
menu: main
---
# Position
I am running for Code for America's [National Advisory Council] (South region).

---
# General Resources
Read the official candidate responses and vote [here][Election Page].

Watch the official [candidate forum][Candidate Forum].

Watch our *short* regional
[undebates]({{< relref "../post/2020-02-12-undebate.md" >}}).

---
# My Brigade
I am currently the [Interim Executive Director at Code for Nashville][Code for Nashville Team].

[Code for Nashville Team]: http://www.codefornashville.org/#team
[Election Page]: https://brigade.codeforamerica.org/about/national-advisory-council/2020-candidates
[National Advisory Council]: https://brigade.codeforamerica.org/about/national-advisory-council
[Candidate Forum]: https://codeforamerica.zoom.us/rec/play/v8YkIr3-qjM3EtWUtgSDU6B6W9S8e6OshHIb_KEMmRvnUHhQOlX0N7FEMLO6gL6OaaacUqxf27utWkQq?continueMode=true
