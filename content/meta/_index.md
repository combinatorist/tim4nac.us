---
title: "Meta"
description: "More than meets the eye..."
type: page
menu: main
---
This site is completely open source.\*
You can fork (i.e. steal) it for your own use or suggest changes [here](https://gitlab.com/combinatorist/tim4nac.us).

\*Even the code hosting platform is open source! That's why I choose Gitlab.com over Github.com.
