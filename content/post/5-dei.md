---
title: "5. How can the NAC and the Network team support your efforts to make your brigade more diverse and inclusive? Can you speak to what work you've already done in this area?"
date: "2020-01-26"
---
When I joined Brigade leadership 2 years ago, I advocated for a new position of Community & Diversity Lead.
I took the position to diversify our core team of 4 cis white men and improve the experience of our marginalized members.
Now, our core team is smaller and through recruiting, only 1 out of 6 of our project leads is a white male.
After a near Code of Conduct violation from some privileged first-timers, I conducted seven 1-hour interviews of the people involved.
We started an onboarding process to help protect project groups from those disruptions and microaggressions.
I am recruiting for a new position of Latinx Community Liaison to help address underrepresentation in our Brigade and city government.

I am always looking for new ways to identify and counter everything from systemic issues down to my own microaggressions and privilege.
But, I would love help recruiting enough leaders, volunteers, and outside feedback so that we could learn and improve without tokenizing anybody.
