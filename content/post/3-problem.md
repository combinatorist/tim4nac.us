---
title: "3. What do you see as a key problem that the NAC should help the Network solve in the next year?"
date: "2020-01-26"
---
I think the Network's greatest problem is our awkward relationship with CfA.
For example, CfA avidly endorses transparency, but they don't publish NAC election numbers.

I don't think they undermine the Network on purpose - it's just hard for them as a hierarchical (non-profit) consulting company to advocate on behalf of a grassroots volunteer movement.
Brigades were not part of the original CfA model, so this relationship is going to take a lot of work.

But, I think the NAC can use the informal grassroots power of the Network to establish the formal powers it needs to work with CfA and advocate effectively for the Network.
For example, the NAC could transparently collaborate with the Network to publicly propose a budget for the Network team so CfA might include Network representation in future budgets.

Elected or not, I will continue to assume the best of intentions and work with CfA, but only to the extent that I feel works for Brigades.
