---
title: "1. Bio - In fewer than 280 characters, introduce yourself to members of the Brigade Network who may not know you."
date: "2020-01-26"
---
I co-led the unconference portion of the 2019 Brigade Conference.
I split my time running Code for Nashville and doing data consulting for healthcare and government.
I am passionate about DEI, data, transparency, and direct democracy.
Learn more at tim4nac.us. :)
