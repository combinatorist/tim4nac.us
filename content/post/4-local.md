---
title: "4. How will you balance your work with your local Brigade with serving on the National Advisory Council?"
date: "2020-01-26"
---
I worry that changes in CfA leadership may limit the support they give Brigades.

I wasn't planning to run for NAC because I wanted to get Code for Nashville ready in case we needed to operate without CfA.
We just started defining our formal membership so we can start having elections for leadership.
And, I was hoping to start securing grants and contracts to help support stipends and scholarships for additional co-leads and volunteers.

But, several people in other brigades encouraged me to run this year to publicly share these concerns about CfA that I hear many of us share privately.
So, I decided to run and if elected, I will cut back as much as possible on Code for Nashville to advocate for Brigades in the NAC.
Our leadership team feels confident they could cut back on some projects and plans for the future in order to maintain the Brigade for now.

No matter what, I will continue to develop direct Brigade to Brigade relationships because I believe we need each other to thrive.
