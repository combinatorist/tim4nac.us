---
title: "Endorsements"
date: 2020-02-01T16:09:56-06:00
type: page
menu: main
---
Former NAC Members:
- [Chris Alfano](https://twitter.com/themightychris/status/1222248965153918980)
- [Janet Michaelis](https://twitter.com/jgmichaelis/status/1227989983321042946)\*

\*Running in Central region: www.janet4nac.us
